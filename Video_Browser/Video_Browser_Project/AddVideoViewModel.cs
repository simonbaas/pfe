﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;

namespace Video_Browser_Project
{
    /**
     * AddVideoViewModel class
     * View Model for adding video
     */
    public class AddVideoViewModel : INotifyPropertyChanged
    {
        private readonly ConfigReader configReader;
        private readonly MongoDB_Controller mdb;
        private String _selectedVideo;
        private String _selectedVideo_copy;
        private String _choosen_folder;
        private readonly DirectoryInfo destinationDir;
        private ObservableCollection<String> _destinationList;
        private System.Windows.Forms.Timer loading_timer;

        /// <summary>
        /// Constructor of AddVideoViewModel.
        /// This object sets the VideoList, found in the configuration object.
        /// </summary>
        public AddVideoViewModel()
        {
            _destinationList = new ObservableCollection<String>();
            configReader = ConfigReader.GetInstance();
            mdb = MongoDB_Controller.GetInstance();
            destinationDir = new DirectoryInfo(configReader.DestinationDir);
            ToSendVideo = new Video();
            Directories = destinationDir.GetDirectories("*").Where(d => d.Name != "delete");
            foreach (DirectoryInfo directory in Directories)
            {
                _destinationList.Add(directory.Name);
            }
            Init_Loading_Timer();
        }

        /// <summary>
        /// Video objet to be added to the database.
        /// </summary>
        public Video ToSendVideo { get; set; }
        public IEnumerable<DirectoryInfo> Directories { get; }

        /// <summary>
        /// List of the destination directories.
        /// </summary>
        public ObservableCollection<String> Destination_List
        {
            get => _destinationList;
            set
            {
                _destinationList = value;
                RaisePropertyChanged("Destination_List");
            }
        }

        /// <summary>
        /// The name of the destination directory choosen.
        /// </summary>
        public String Choosen_Folder
        {
            get => _choosen_folder;
            set
            {
                _choosen_folder = value;
                RaisePropertyChanged("Choosen_Folder");
            }
        }

        /// <summary>
        /// The selectedVideo, it will be the path of the video to enter in the database.
        /// </summary>
        public String Selected_Video
        {
            get => _selectedVideo;
            set
            {
                _selectedVideo = value;
                if (_selectedVideo != null)
                {
                    _selectedVideo_copy = String.Copy(_selectedVideo);
                }
                Start_Loading_Timer();
                RaisePropertyChanged("Selected_Video");
            }
        }

        /// <summary>
        /// This is the source path of the selected file.
        /// </summary>
        public String Selected_Path
        {
            get
            {
                if (_selectedVideo_copy == null)
                {
                    return "";
                }
                else
                {
                    return _selectedVideo_copy;
                }
            }
            set
            {

            }
        }

        /// <summary>
        /// The usual PropertyChanged event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICommand Add_Video => new RelayCommand(AddVideo);

        /// <summary>
        /// The counter for the timer.
        /// </summary>
        private static int _counter_before_load_video = 2;

        /// <summary>
        /// Send the property changed event for Selected_Path and Selected_Video.
        /// </summary>
        private void Force_PropertyChanged()
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler(this, new PropertyChangedEventArgs("Selected_Path"));
            handler(this, new PropertyChangedEventArgs("Selected_Video"));
        }

        public void Call_Video_Load()
        {
            Call_Video_Load(false);
        }

        /// <summary>
        /// Call the video and raise properties in consequence.
        /// </summary>
        /// <param name="Bypass_Counter"></param>
        public void Call_Video_Load(bool Bypass_Counter)
        {
            if (Bypass_Counter)
            {
                Force_PropertyChanged();
            }
            else
            {
                RaisePropertyChanged("Selected_Video");
                RaisePropertyChanged("Selected_Path");
            }
        }

        /// <summary>
        /// Initialize the loading timer.
        /// </summary>
        private void Init_Loading_Timer()
        {
            loading_timer = new System.Windows.Forms.Timer();
            loading_timer.Tick += new EventHandler(Check_Loading);
            loading_timer.Interval = 200; // in miliseconds
        }

        /// <summary>
        /// Start the loading timer.
        /// </summary>
        private void Start_Loading_Timer()
        {
            _counter_before_load_video = 2;
            if (!loading_timer.Enabled)
            {
                loading_timer.Start();
            }
        }

        /// <summary>
        /// Check is the object is currently load.
        /// </summary>
        /// <param name="sender">The object to be check</param>
        /// <param name="e">The event to be apply</param>
        private void Check_Loading(object sender, EventArgs e)
        {
            if (_counter_before_load_video <= 0)
            {
                Call_Video_Load();
                loading_timer.Stop();
            }
            else
            {
                _counter_before_load_video--;
            }
        }

        /// <summary>
        /// Function for adding videos in MongoDB database.
        /// </summary>
        public void AddVideo()
        {
            bool year_is_ok = ToSendVideo.Year == "" || int.TryParse(ToSendVideo.Year, out _);
            bool title_is_ok = ToSendVideo.Title.Trim() != "";
            bool file_is_ok = Selected_Path != "";
            if (title_is_ok && file_is_ok && year_is_ok)
            {
                string path = Selected_Path;
                string videoFileName = _selectedVideo_copy.Split('\\').Last();
                ToSendVideo.Path = configReader.DestinationDir + @"\" + Choosen_Folder + @"\" + videoFileName;
                try
                {
                    File.Copy(Selected_Path, ToSendVideo.Path, true);
                }
                catch (System.IO.FileNotFoundException)
                {
                    MessageBox.Show("Le chemin du fichier est introuvable", "La vidéo n'a pas été ajoutée", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                _selectedVideo_copy = null;
                Call_Video_Load(true);
                mdb.Put_Video(ToSendVideo);
                ToSendVideo = new Video();
                RaisePropertyChanged("ToSendVideo");
                try
                {
                    Thread.Sleep(500);
                    File.Delete(path);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "\n" + e.StackTrace);
                }
            }
            else
            {
                String message;
                if (!file_is_ok)
                {
                    message = "Aucune vidéo n'est sélectionnée";
                }
                else
                {
                    message = "Certains champs n'ont pas été remplis : \n";
                    if (!title_is_ok)
                    {
                        message += "\tLe titre est manquant\n";
                    }
                    if (!year_is_ok)
                    {
                        message += "\tL'année doit être en chiffres\n";
                    }
                }
                MessageBox.Show(message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
