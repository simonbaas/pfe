﻿using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;


namespace Video_Browser_Project
{
    /// <summary>
    /// Logique d'interaction pour Video_Modification.xaml
    /// </summary>
    public partial class Video_Modification : Window
    {
        public Video_Modification(Video video, ListVideoViewModel lvvm)
        {
            InitializeComponent();
            DataContext = new VideoViewModel(video, lvvm);
            /*VlcPlayer.Play_Video(video);*/
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            VideoViewModel context = (VideoViewModel)DataContext;
            context.On_Quit();
            VlcPlayer.StopPlayer();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            VideoViewModel context = (VideoViewModel)DataContext;
            context.Delete_Video(this);
        }
    }
}
