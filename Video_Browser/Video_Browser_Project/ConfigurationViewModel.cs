﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;

namespace Video_Browser_Project
{
    /**
     * ConfigurationViewModel class
     * Contains all the method for the configuration view
     **/
    public class ConfigurationViewModel : INotifyPropertyChanged
    {
        private readonly ConfigReader configReader;
        private readonly MongoDB_Controller mdb;

        public ConfigurationViewModel()
        {
            configReader = ConfigReader.GetInstance();
            mdb = MongoDB_Controller.GetInstance();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Path of the destination directory.
        /// </summary>
        public string DestDir
        {
            get => configReader.DestinationDir;
            set
            {
                configReader.DestinationDir = value;
                RaisePropertyChanged("DestDir");
            }
        }

        /// <summary>
        /// Path of the delete video directory.
        /// </summary>
        public string DeleteDir
        {
            get => configReader.DeleteDir;
            set
            {
                configReader.DeleteDir = value;
                RaisePropertyChanged("DeleteDir");
            }
        }

        /// <summary>
        /// Open a FileSelect Dialog for the user, he can choose a directory.
        /// </summary>
        /// <returns></returns>
        private string GetDirectory()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog()
            {
                IsFolderPicker = true
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            CommonFileDialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == CommonFileDialogResult.Ok)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Open a File Dialog for the user, he can choose an XML file to open.
        /// </summary>
        /// <returns>The XML file name.</returns>
        private string Get_XML()
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                AddExtension = true,
                Filter = "(*.xml)|*.xml"
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            DialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == DialogResult.OK)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Open a File Dialog for the user, he will give the filename he want to save the XML.
        /// </summary>
        /// <returns>The XML file name.</returns>
        private string Get_Savefile_XML()
        {
            SaveFileDialog dialog = new SaveFileDialog()
            {
                AddExtension = true,
                Filter = "(*.xml)|*.xml"
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            DialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == DialogResult.OK)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        public ICommand ChangeDest => new RelayCommand(ChangeDestDir);

        public ICommand ChangeDel => new RelayCommand(ChangeDeleteDir);

        public ICommand Export => new RelayCommand(Export_Base);

        public ICommand Import => new RelayCommand(Import_Base);

        public ICommand EmptyBase => new RelayCommand(Empty_Base);

        /// <summary>
        /// Clean the database, removing all entries.
        /// </summary>
        private void Empty_Base()
        {
            DialogResult choice = System.Windows.Forms.MessageBox.Show("Cette action videra complètement la base de données de l'application, voulez-vous continuer ?", "Vider la base", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (choice == DialogResult.Yes)
            {
                mdb.CleanDatabase();
                System.Windows.Forms.MessageBox.Show("Suppression de la base réussie", "Base vidée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Takes all videos in the database and put them in a XML file.
        /// </summary>
        private void Export_Base()
        {
            string xmlFile = Get_Savefile_XML();
            if (xmlFile != null)
            {
                List<Video> video_list = mdb.Search_Video();
                XmlDocument xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlElement root = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, root);
                XmlElement videos = xmlDoc.CreateElement("video_list");
                foreach (Video video in video_list)
                {
                    XmlElement video_node = xmlDoc.CreateElement("video");
                    XmlElement title = xmlDoc.CreateElement("title");
                    XmlText titleText = xmlDoc.CreateTextNode(video.Title);
                    XmlElement year = xmlDoc.CreateElement("year");
                    XmlText yearText = xmlDoc.CreateTextNode(video.Year);
                    XmlElement path = xmlDoc.CreateElement("path");
                    XmlText pathText = xmlDoc.CreateTextNode(video.Path);
                    XmlElement desc = xmlDoc.CreateElement("desc");
                    XmlText descText = xmlDoc.CreateTextNode(video.Description);
                    title.AppendChild(titleText);
                    year.AppendChild(yearText);
                    path.AppendChild(pathText);
                    desc.AppendChild(descText);
                    video_node.AppendChild(title);
                    video_node.AppendChild(path);
                    video_node.AppendChild(year);
                    video_node.AppendChild(desc);
                    videos.AppendChild(video_node);
                }
                xmlDoc.AppendChild(videos);
                xmlDoc.Save(xmlFile);
            }
        }

        /// <summary>
        /// From an XML file, deploy all videos in it, in the database.
        /// </summary>
        private void Import_Base()
        {
            string xmlFile = Get_XML();
            if (xmlFile != null)
            {
                XmlDocument doc = new XmlDocument();
                XmlReader xmlReader = XmlReader.Create(xmlFile);
                doc.Load(xmlReader);
                XmlNodeList xmlNodeList = doc.FirstChild.NextSibling.ChildNodes;

                foreach (XmlNode node in xmlNodeList)
                {
                    XmlNode Title = node.SelectSingleNode("title/text()");
                    XmlNode Year = node.SelectSingleNode("year/text()");
                    XmlNode Path = node.SelectSingleNode("path/text()");
                    XmlNode Description = node.SelectSingleNode("desc/text()");
                    string title_text = "";
                    string year_text = "";
                    string path_text = "";
                    string desc_text = "";
                    if (Title != null)
                    {
                        title_text = Title.Value;
                    }
                    if (Year != null)
                    {
                        year_text = Year.Value;
                    }
                    if (Path != null)
                    {
                        path_text = Path.Value;
                    }
                    if (Description != null)
                    {
                        desc_text = Description.Value;
                    }
                    Video video = new Video(title_text, year_text, desc_text, path_text);
                    mdb.Put_Video(video);
                }
                System.Windows.Forms.MessageBox.Show("L'importation a réussie", "Importation réussite", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Show a dialog for choosing dir the replace the current DestDir by the new one.
        /// </summary>
        public void ChangeDestDir()
        {
            string dir = GetDirectory();
            if (dir != null && Directory.Exists(dir))
            {
                DestDir = dir;
            }
        }

        /// <summary>
        /// Show a dialog for choosing dir the replace the current DeleteDir by the new one.
        /// </summary>
        public void ChangeDeleteDir()
        {
            string dir = GetDirectory();
            if (dir != null && Directory.Exists(dir))
            {
                DeleteDir = dir;
            }
        }
    }
}
