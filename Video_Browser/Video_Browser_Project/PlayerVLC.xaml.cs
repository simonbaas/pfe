﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Video_Browser_Project
{
    /// <summary>
    /// This is the VLC Player embed in the app
    /// </summary>
    public partial class PlayerVLC : System.Windows.Controls.UserControl
    {
        private Timer hide_control_timer;
        private Timer progress_bar_timer;
        private int seconds_before_hiding_control;
        private FullScreenVLC fullScreen;

        /// <summary>
        /// PlayerVLC constructor, initialise the VLC Player with the path of the VLC libraries (libvlc in folder).
        /// </summary>
        public PlayerVLC()
        {
            InitializeComponent();
            Init_Player();
            ControlBarBackground.Opacity = 0;
            seconds_before_hiding_control = 0;
            videoProgression.Value = 0;
            Init_ControlBar_Timer();
            Init_ProgressBar_Timer();
        }

        public bool IsFullScreen
        {
            get;
            set;
        }

        /// <summary>
        /// The path of the video to play, when the video is set, reset the video_player with the new video.
        /// </summary>
        private string _videoPath;

        public string VideoPath
        {
            get => _videoPath;
            set
            {
                _videoPath = value;
                if (value != "" && File.Exists(value))
                {
                    progress_bar_timer.Stop();
                    Console.WriteLine("Playing : " + new FileInfo(value));
                    VideoControl.SourceProvider.MediaPlayer.Play(new FileInfo(value));
                    progress_bar_timer.Start();
                    Actualize_Play_Image();
                }
                else
                {
                    System.Threading.Thread thread = new System.Threading.Thread(delegate () { VideoControl.SourceProvider.MediaPlayer.Stop(); });
                    thread.Start();
                }
            }
        }

        /// <summary>
        /// Initialize the player.
        /// </summary>
        public void Init_Player()
        {
            Assembly currentAssembly = Assembly.GetEntryAssembly();
            string currentDirectory = new FileInfo(currentAssembly.Location).DirectoryName;
            DirectoryInfo vlcLibDirectory = new DirectoryInfo(System.IO.Path.Combine(currentDirectory, "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));
            string[] options = new string[]
            {
                // Put VLC options here
            };
            VideoControl.SourceProvider.CreatePlayer(vlcLibDirectory, options);
        }

        /// <summary>
        /// Close the player.
        /// </summary>
        public void StopPlayer()
        {
            VideoControl?.Dispose();
            hide_control_timer.Stop();
            if (progress_bar_timer != null)
            {
                progress_bar_timer.Stop();
            }
        }

        // VIDEO

        /// <summary>
        /// Play a video from a Video object.
        /// </summary>
        /// <param name="video">the Video object to play</param>
        public void Play_Video(Video video)
        {
            try
            {
                if (video != null)
                {
                    VideoPath = video.Path;
                    Actualize_Play_Image();
                }
                else
                {
                    System.Windows.MessageBox.Show("Video non chargée");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                System.Windows.MessageBox.Show("Video non chargée");
            }
        }

        /// <summary>
        /// Move to the selected time of the video, with a percentage.
        /// </summary>
        /// <param name="percent">Percent video's timer</param>
        private void Change_Video_Time(double percent)
        {
            if (VideoPath != null)
            {
                System.Threading.Thread.Sleep(100);
                VideoControl.SourceProvider.MediaPlayer.Time = (long)(percent * VideoControl.SourceProvider.MediaPlayer.Length);
            }
        }

        /// <summary>
        /// Move to the selected time of the video, with a long as parameter.
        /// </summary>
        /// <param name="time">Video's timer</param>
        public void Change_Video_Time(long time)
        {
            if (VideoPath != null)
            {
                System.Threading.Thread.Sleep(10);
                VideoControl.SourceProvider.MediaPlayer.Time = time;
            }
        }

        /// <summary>
        /// Init a timer for launching the function Hide_Control at each tick.
        /// </summary>
        private void Init_ControlBar_Timer()
        {
            hide_control_timer = new Timer();
            hide_control_timer.Tick += new EventHandler(Hide_Control);
            hide_control_timer.Interval = 1; // in miliseconds
            hide_control_timer.Start();
        }

        /// <summary>
        /// Reset the counter for hiding the controls when the mouse is moved
        /// </summary>
        private void Grid_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            seconds_before_hiding_control = 100;
        }

        /// <summary>
        /// Event that Hide all control when the mouse don't move and make them appear again when the mouse move.
        /// </summary>
        private void Hide_Control(object sender, EventArgs e)
        {
            if (seconds_before_hiding_control <= 0)
            {
                Play.Visibility = Visibility.Collapsed;
                ControlBarBackground.Visibility = Visibility.Collapsed;
                videoProgression.Visibility = Visibility.Collapsed;
                FullScreen.Visibility = Visibility.Collapsed;
                VideoTime.Visibility = Visibility.Collapsed;
                if (IsFullScreen)
                {
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.None;
                }
            }
            else
            {
                seconds_before_hiding_control--;
                Play.Visibility = Visibility.Visible;
                ControlBarBackground.Visibility = Visibility.Visible;
                videoProgression.Visibility = Visibility.Visible;
                FullScreen.Visibility = Visibility.Visible;
                VideoTime.Visibility = Visibility.Visible;
                if (IsFullScreen)
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        // PLAY/PAUSE BUTTON

        /// <summary>
        /// Actualize the button image when called. 
        /// If the video is paused, then the image will be a play image, else, it will be a pause image.
        /// </summary>
        private void Actualize_Play_Image()
        {
            if (VideoControl.SourceProvider.MediaPlayer.IsPlaying())
            {
                Play.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/Images/playVLC.PNG"));
            }
            else
            {
                Play.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/Images/pauseVLC.PNG"));
            }
        }

        /// <summary>
        /// Pause the video when clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Toggle_Pause_Event(object sender, MouseButtonEventArgs e)
        {
            if (VideoControl.SourceProvider.MediaPlayer.IsPlaying())
            {
                VideoControl.SourceProvider.MediaPlayer.Pause();
                Actualize_Play_Image();
            }
            else
            {
                VideoControl.SourceProvider.MediaPlayer.Play();
                Actualize_Play_Image();
            }
        }

        // PROGRESS BAR

        /// <summary>
        /// This function init the timer for the progressBar, this one will actualize the filling of the bar.
        /// </summary>
        private void Init_ProgressBar_Timer()
        {
            progress_bar_timer = new Timer();
            progress_bar_timer.Tick += new EventHandler(ChangeProgressBar);
            progress_bar_timer.Interval = 1; // in miliseconds
            progress_bar_timer.Start();
        }

        /// <summary>
        /// Actualize the progresssion bar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeProgressBar(object sender, EventArgs e)
        {
            videoProgression.Maximum = VideoControl.SourceProvider.MediaPlayer.Length;
            long result = VideoControl.SourceProvider.MediaPlayer.Time;
            Actualize_Time();
            videoProgression.Value = (int)result + 1;
        }

        /// <summary>
        /// Event when video progression is clicked.
        /// Change the time in the video to the relative position where the bar has been clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VideoProgression_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IInputElement bar = (IInputElement)sender;
            Point point = e.GetPosition(bar);
            double totalSize = videoProgression.ActualWidth;
            double position = point.X;
            Change_Video_Time((position / totalSize));
        }

        // TIME DISPLAYER

        /// <summary>
        /// Actualize the video time.
        /// </summary>
        private void Actualize_Time()
        {
            long sec = VideoControl.SourceProvider.MediaPlayer.Time / 1000;
            long secMax = VideoControl.SourceProvider.MediaPlayer.Length / 1000;
            VideoTime.Content = (sec / 60).ToString("00") + ":" + (sec % 60).ToString("00") + "/" + (secMax / 60).ToString("00") + ":" + (secMax % 60).ToString("00");
        }

        // FULLSCREEN

        /// <summary>
        /// Launch a fullScreen window, setting the video to play, the time to start and pause the actual player
        /// When fullscreen is closed, Reset the cursor (because it can disappear in the fullscreen Window), recover the time of the fullScreen Window and Relaunch the video.
        /// </summary>
        public void GoFullscreen()
        {
            bool isPlaying = VideoControl.SourceProvider.MediaPlayer.IsPlaying();
            long time = VideoControl.SourceProvider.MediaPlayer.Time;
            if (isPlaying) // Pause the MediaPlayer in background
            {
                VideoControl.SourceProvider.MediaPlayer.Pause();
                Actualize_Play_Image();
            }
            fullScreen = new FullScreenVLC(VideoPath, time);
            /*fullScreen.Show();*/
            fullScreen.ShowDialog();
            Mouse.OverrideCursor = null;
            /*fullScreen.Change_Video_Time_FullScreen(fullScreen.TimeOnStop);*/
            Change_Video_Time(fullScreen.TimeOnStop);
            if (isPlaying) // Keep the same state before when enter in the fullscreen
            {
                VideoControl.SourceProvider.MediaPlayer.Play();
                Actualize_Play_Image();
            }
            /*if (!isPlaying) // Keep the same state before when enter in the fullscreen
            {
                fullScreen.PlayFullScreen();
                Actualize_Play_Image();
                Change_Video_Time(fullScreen.TimeOnStop);
                VideoControl.SourceProvider.MediaPlayer.Pause();
            }*/
        }

/*        public void LeaveFullScrean()
        {
            bool isPlaying = VideoControl.SourceProvider.MediaPlayer.IsPlaying();
            if (!isPlaying) // Keep the same state before when enter in the fullscreen
            {
                Change_Video_Time(fullScreen.TimeOnStop);
                VideoControl.SourceProvider.MediaPlayer.Pause();
            }
        }*/

        /// <summary>
        /// Launched when fullScreen button is pressed, Start Fullscreen when it's an embed video, close fullscreen if it is already a fullscreen video
        /// </summary>
        private void Toggle_Fullscreen_Event(object sender, MouseButtonEventArgs e)
        {
            /*bool isPlaying = VideoControl.SourceProvider.MediaPlayer.IsPlaying();*/
            if (!IsFullScreen)
            {
                GoFullscreen();
            }
            else
            {
                Image s = (Image)sender;
                Window.GetWindow(s).Close();
                /*LeaveFullScrean();*/
            }
        }

        // PARAMETERS IN XAML : used parameters from the XAML lines

        /// These static functions allow to integrete a "videoPath" parameter in the XAML of the Player
        public static readonly DependencyProperty VideoPathProperty = DependencyProperty.Register("VideoPath", typeof(string), typeof(PlayerVLC), new PropertyMetadata("", new PropertyChangedCallback(OnPathChanged)));

        public static readonly DependencyProperty FullScreenProperty = DependencyProperty.Register("IsFullScreen", typeof(bool), typeof(PlayerVLC), new PropertyMetadata(false, new PropertyChangedCallback(OnFullScreenChanged)));

        /// <summary>
        /// Static methods for calling local method of player.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PlayerVLC player = d as PlayerVLC;
            player.OnPathChanged(e);
        }

        /// <summary>
        /// Action to perform when the property "IsFullScreen" is changed
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnFullScreenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PlayerVLC player = d as PlayerVLC;
            player.OnFullScreenChanged(e);
        }

        /// <summary>
        /// Change to make to VideoPath when the path is changed
        /// </summary>
        /// <param name="e">The path changed</param>
        private void OnPathChanged(DependencyPropertyChangedEventArgs e)
        {
            VideoPath = e.NewValue.ToString();
        }

        /// <summary>
        /// Set a value if the player is fullscreen or not.
        /// </summary>
        /// <param name="e">IsFullScreen property</param>
        private void OnFullScreenChanged(DependencyPropertyChangedEventArgs e)
        {
            IsFullScreen = (bool)e.NewValue;
        }
    }
}
