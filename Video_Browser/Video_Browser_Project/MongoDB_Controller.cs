﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Video_Browser_Project
{
    /**
     * MongoDB_Controller class
     * A Class who control MongoDB providing functions to put and recover videos.
     */
    internal class MongoDB_Controller
    {
        private static MongoDB_Controller instance = null;
        private readonly static string DATABASE = "video_browser";
        private readonly static string COLLECTION = "video";
        private static IMongoCollection<Video> collection;
        private static IMongoDatabase dataBase;
        private static IMongoDatabase adminDataBase;
        private static MongoClient client;

        /// <summary>
        /// Default constructor for MongoDB_Controller
        /// </summary>
        private MongoDB_Controller()
        {
            client = new MongoClient();
            dataBase = client.GetDatabase(DATABASE);
            CreateVideoCollection();
            collection = dataBase.GetCollection<Video>(COLLECTION);
        }

        /// <summary>
        /// MongoDB_Controller constructor, can be used to change the port.
        /// </summary>
        /// <param name="port">The port to be change if the default one is used</param>
        private MongoDB_Controller(int port)
        {
            client = new MongoClient("mongodb://localhost:" + port);
            dataBase = client.GetDatabase(DATABASE);
        }

        /// <summary>
        /// Implement the singleton pattern.
        /// </summary>
        /// <returns>A single instance of the class.</returns>
        public static MongoDB_Controller GetInstance()
        {
            if (instance == null)
            {
                instance = new MongoDB_Controller();
            }
            return instance;
        }

        // DATABASE FUNCTIONS

        /// <summary>
        /// Check if the collection exists in the datanase
        /// </summary>
        /// <param name="database">The database name</param>
        /// <param name="collectionName">The collection name</param>
        /// <returns></returns>
        private bool CollectionExists(IMongoDatabase database, string collectionName)
        {
            BsonDocument filter = new BsonDocument("name", collectionName);
            ListCollectionNamesOptions options = new ListCollectionNamesOptions { Filter = filter };
            return database.ListCollectionNames(options).Any();
        }

        /// <summary>
        /// Create the collection if this one doesn't exist.
        /// </summary>
        private void CreateVideoCollection()
        {
            if (!CollectionExists(dataBase, COLLECTION))
            {
                dataBase.CreateCollection(COLLECTION);
            }
        }

        /// <summary>
        /// Clean the database.
        /// </summary>
        public void CleanDatabase()
        {
            dataBase.DropCollection(COLLECTION);
        }

        /// <summary>
        /// Shutdown the database.
        /// </summary>
        public void Shutdown()
        {
            adminDataBase = client.GetDatabase("admin");
            adminDataBase.RunCommandAsync<BsonDocument>(new JsonCommand<BsonDocument>("{shutdown: 1}"));
        }

        // ITEM FUNCTIONS

        /// <summary>
        /// Modify the choosen video.
        /// </summary>
        /// <param name="videoToReplace">The video to be modified</param>
        /// <param name="newInstance">The new video</param>
        public void Modify_Video(Video videoToReplace, Video newInstance)
        {
            Delete_Video(videoToReplace);
            Put_Video(newInstance);
        }

        /// <summary>
        /// Delete the video from the database.
        /// </summary>
        /// <param name="video">The video to be deleted</param>
        public void Delete_Video(Video video)
        {
            FilterDefinitionBuilder<Video> builder = Builders<Video>.Filter;
            FilterDefinition<Video> video_filter = builder.Eq("Path", video.Path);
            collection.DeleteOne(video_filter);
        }

        /// <summary>
        /// Put a video in the database
        /// </summary>
        /// <param name="video">The video to be put</param>
        public void Put_Video(Video video)
        {
            collection.InsertOne(video);
        }

        /// <summary>
        /// Search videos in the database.
        /// </summary>
        /// <returns>The default list video</returns>
        internal List<Video> Search_Video()
        {
            return collection.Find<Video>(FilterDefinition<Video>.Empty).ToList<Video>();
        }

        /// <summary>
        /// Search video with a filter.
        /// </summary>
        /// <param name="filter">The filter to be applied during the research.</param>
        /// <remarks>
        /// <para>If the filter is composed of two words splitted by a space character, a permutation will be made.</para>
        /// <para>This functionnality allow the user to just put words and the search will match wherever the word is located.</para>
        /// </remarks>
        /// <example>
        /// For two title :
        ///    - Dog with cat playing around
        ///    - Cat fighting a Dog
        /// If the user's search is : "dog cat"
        /// The search will be format into "(.*dog.*cat.*)|(.*cat.*dog.*)"
        /// And will match with the two videos.
        /// If the search was just format into ".*dog.*cat.*" it would only match with the first video.
        /// </example>
        /// <returns>The list of the video that match research's criteria.</returns>
        public List<Video> Search_Video(string filter)
        {
            FilterDefinitionBuilder<Video> builder = Builders<Video>.Filter;
            string[] splitted_filter = filter.Trim().Split(' ');
            string permuted_string = "";
            if (splitted_filter.Length != 0)
            {
                List<List<string>> permuted_filter = Permut_List(new List<string>(splitted_filter));
                permuted_string += "(.*";
                int permut_index;
                for (permut_index = 0; permut_index < permuted_filter.Count - 1; permut_index++)
                {
                    permuted_string += string.Join(".*", permuted_filter[permut_index]) + ".*)|(.*";
                }
                permuted_string += string.Join(".*", permuted_filter[permut_index]) + ".*)";
            }
            else
            {
                permuted_string = filter;
            }

            FilterDefinition<Video> video_filter = builder.Regex("Title", new BsonRegularExpression(permuted_string, "i"))
                | builder.Regex("Description", new BsonRegularExpression(permuted_string, "i"))
                | builder.Regex("Year", new BsonRegularExpression(permuted_string, "i"));
            return collection.Find(video_filter).ToList();
        }

        /// <summary>
        /// Recursive permutation of list.
        /// </summary>
        /// <param name="string_list_to_permut">The list of string to be permuted.</param>
        /// <returns>All of the possible permutation.</returns>
        public List<List<string>> Permut_List(List<String> string_list_to_permut)
        {
            List<List<string>> result = new List<List<string>>();
            List<string> one_list = new List<string>();
            if (string_list_to_permut.Count == 0)
            {
                return result;
            }
            if (string_list_to_permut.Count == 1)
            {
                result.Add(string_list_to_permut);
                return result;
            }
            if (string_list_to_permut.Count == 2)
            {
                one_list.Add(string_list_to_permut[1]);
                one_list.Add(string_list_to_permut[0]);
                result.Add(string_list_to_permut);
                result.Add(one_list);
                return result;
            }
            else
            {
                string first_occurence = string_list_to_permut[0];
                string_list_to_permut.RemoveAt(0);
                List<List<string>> result_permut = Permut_List(string_list_to_permut);
                foreach (List<string> permutation in result_permut)
                {
                    for (int position = 0; position <= permutation.Count; position++)
                    {
                        one_list = new List<string>(permutation);
                        one_list.Insert(position, first_occurence);
                        result.Add(one_list);
                    }
                }
                return result;
            }
        }
    }
}
