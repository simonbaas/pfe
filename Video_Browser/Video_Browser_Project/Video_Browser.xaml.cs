﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace Video_Browser_Project
{
    /// <summary>
    /// Logique d'interaction pour Video_Browser.xaml
    /// </summary>
    public partial class Video_Browser : Window
    {
        private Process mongod;

        //private MongoDB_Controller mdb;
        //private const int ERROR_CANCELLED = 1223;
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;

        /// <summary>
        /// Check if the default video directory exist, then launch mongodb and init the window.
        /// </summary>
        public Video_Browser()
        {
            if (!Directory.Exists("videos"))
            {
                Directory.CreateDirectory("videos");
            }
            //Launch_Mongo();
            InitializeComponent();
        }

        /// <summary>
        /// Lancement de Mongodb, si Mongodb n'existe pas, dezip l'archive de mongodb qui aura été ajoutée à l'installation
        /// puis la supprime pour laisser de la place.
        /// </summary>
        public void Launch_Mongo()
        {
            if (File.Exists("mongodb/bin/mongod.exe"))
            {
                if (!File.Exists("mongodb/bin/database"))
                {
                    File.Create("mongodb/bin/database");
                }
            }
            else
            {
                if (File.Exists("mongodb.zip"))
                {
                    ZipFile.ExtractToDirectory("mongodb.zip", ".");
                    File.Delete("mongodb.zip");
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Mongodb n'a pas été trouvé");
                    System.Windows.Application.Current.Shutdown();
                }

            }
            mongod = new Process();
            FileInfo fileInfo = new FileInfo("mongodb/bin/mongod.exe");
            System.Console.WriteLine(fileInfo.Exists);
            mongod.StartInfo = new ProcessStartInfo(fileInfo.FullName);
            mongod.StartInfo.UseShellExecute = false;
            //mongod.StartInfo.Arguments = "--dbpath C:/data";
            mongod.StartInfo.CreateNoWindow = true;
            mongod.Start();
        }

        /// <summary>
        /// Function called when the window is closed, shutdown mongodb.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            /*mdb = MongoDB_Controller.GetInstance();
                mdb.Shutdown();
            try
            {
                mongod.Kill();
            }
            catch (System.InvalidOperationException)
            {

            }*/
        }

        /// <summary>
        /// Function called when an item in the list is clicked, launch the function "Open_Video" of ListVideoViewModel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenVideo(object sender, MouseButtonEventArgs e)
        {
            if (videoList.SelectedIndex != -1)
            {
                ListVideoViewModel lvvm = (ListVideoViewModel)DataContext;
                lvvm.Start_Loading_Timer();
            }
        }

        /// <summary>
        /// Function called when a right click on an item is made, launch the function "Copy_Link" of ListVideoViewModel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyPath(object sender, MouseButtonEventArgs e)
        {
            if (videoList.SelectedIndex != -1)
            {
                ListVideoViewModel lvvm = (ListVideoViewModel)DataContext;
                lvvm.Copy_Link();
            }
        }

        /// <summary>
        /// Call the purge function on the button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurgeButton_Click(object sender, RoutedEventArgs e)
        {
            if (videoList.SelectedItems.Count != 0)
            {
                List<Video> selectedItems = new List<Video>();
                foreach (Video item in videoList.SelectedItems)
                {
                    selectedItems.Add(item);
                }
                ListVideoViewModel lvvm = (ListVideoViewModel)DataContext;
                lvvm.Purge_Selected_Videos_Folder(selectedItems);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Aucune vidéo n'est selectionnée", "Erreur d'exportation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

        }

        /// <summary>
        /// Filter the clicked column.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VideoListColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                videoList.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            videoList.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }
    }

    /// <summary>
    /// Class which serve to the row filter tab
    /// </summary>
    public class SortAdorner : Adorner
    {
        private readonly static Geometry ascGeometry =
            Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        private readonly static Geometry descGeometry =
            Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement element, ListSortDirection dir)
            : base(element)
        {
            this.Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            TranslateTransform transform = new TranslateTransform
                (
                    AdornedElement.RenderSize.Width - 15,
                    (AdornedElement.RenderSize.Height - 5) / 2
                );
            drawingContext.PushTransform(transform);

            Geometry geometry = ascGeometry;
            if (this.Direction == ListSortDirection.Descending)
                geometry = descGeometry;
            drawingContext.DrawGeometry(Brushes.Black, null, geometry);

            drawingContext.Pop();
        }
    }
}
