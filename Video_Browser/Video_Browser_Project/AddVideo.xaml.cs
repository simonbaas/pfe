﻿using System.Windows;
using MessageBox = System.Windows.Forms.MessageBox;
using DataFormats = System.Windows.Forms.DataFormats;
using MessageBoxButtons = System.Windows.Forms.MessageBoxButtons;
using MessageBoxIcon = System.Windows.Forms.MessageBoxIcon;

namespace Video_Browser_Project
{
    /// <summary>
    /// Logique d'interaction pour AddVideo.xaml
    /// </summary>
    public partial class AddVideo : Window
    {
        public AddVideo()
        {
            InitializeComponent();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            VlcPlayer.StopPlayer();
            base.OnClosing(e);
        }

        /// <summary>
        /// Enters the element into the ListBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
        }

        /// <summary>
        /// Drops the element into the ListBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBox1_Drop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            foreach (string file in files)
            {
                if (!listBox1.Items.Contains(file))
                {
                    listBox1.Items.Add(file);
                }
                else
                {
                    MessageBox.Show("La vidéo est déjà présente", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        /// <summary>
        /// Remove the selected element when the video is added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddVideoButton_Click(object sender, RoutedEventArgs e)
        {
            string title = textBoxTitle.Text;
            if (title != "")
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }
    }
}
