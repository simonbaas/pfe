﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace Video_Browser_Project
{
    /**
     * VideoViewModel class
     * Contains all of the method to edit the selected video
     */
    public class VideoViewModel : INotifyPropertyChanged
    {
        private static ConfigReader configReader;
        private static MongoDB_Controller mdb;
        private Video _videoItem;
        private Video _videoModif;
        private readonly ListVideoViewModel lvvm;

        public VideoViewModel()
        {

        }

        /// <summary>
        /// VideoViewModel constructor.
        /// </summary>
        /// <param name="video">The video selected.</param>
        /// <param name="video_browser">The video list that need to be update.</param>
        public VideoViewModel(Video video, ListVideoViewModel video_browser)
        {
            VideoItem = video;
            VideoModif = VideoItem.Create_Copy();
            lvvm = video_browser;
            configReader = ConfigReader.GetInstance();
            mdb = MongoDB_Controller.GetInstance();
        }

        /// <summary>
        /// The current video.
        /// </summary>
        public Video VideoItem
        {
            get => _videoItem;
            set
            {
                _videoItem = value;
                RaisePropertyChanged("VideoItem");
            }
        }

        /// <summary>
        /// Attribute for the video modification.
        /// </summary>
        public Video VideoModif
        {
            get => _videoModif;
            set
            {
                _videoModif = value;
                RaisePropertyChanged("VideoModif");
            }
        }

        /// <summary>
        /// Title of the video modified.
        /// </summary>
        public string Title
        {
            get => VideoModif.Title;
            set
            {
                VideoModif.Title = value;
                RaisePropertyChanged("Title");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICommand ChangeVideo => new RelayCommand(Change_Video);

        public ICommand MoveVideo => new RelayCommand(Move_Video);

        public ICommand RelinkVideo => new RelayCommand(Relink_Video);

        /// <summary>
        /// Change the video's path (button : "Modifier la vidéo liée à la fiche").
        /// </summary>
        private void Relink_Video()
        {
            String file = Choose_File();
            if (file != "")
            {
                String oldPath = VideoModif.Path;
                if (file != oldPath)
                {
                    DialogResult result = System.Windows.Forms.MessageBox.Show("Vous êtes sur le point de relier la fiche à une autre vidéo :" + file + "\n\nConfirmer ?", "Changement de vidéo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (result == DialogResult.Yes)
                    {
                        DialogResult delete_choice = DialogResult.None;
                        if (VideoItem.Readable)
                        {
                            delete_choice = System.Windows.Forms.MessageBox.Show("Voulez-vous conserver l'ancienne vidéo ?", "Conserver la vidéo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                        }
                        if (delete_choice != DialogResult.Cancel)
                        {

                            Video tmpVideo = VideoItem.Create_Copy();
                            tmpVideo.Path = file;
                            Change_Video(tmpVideo, false);
                            if (delete_choice == DialogResult.Yes)
                            {
                                FileInfo fileInfo = new FileInfo(oldPath);
                                File.Copy(oldPath, configReader.DeleteDir + "/" + fileInfo.Name, true);
                                File.Delete(oldPath);
                            }
                            else if (delete_choice == DialogResult.No)
                            {
                                File.Delete(oldPath);
                            }
                        }
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("La vidéo est identique, veuillez en selectionner une autre", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        /// <summary>
        /// Move the video in another folder (button : "Déplacer la vidéo").
        /// </summary>
        private void Move_Video()
        {
            string folder = Choose_Folder();
            if (folder != "")
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show("Vous êtes sur le point de déplacer la vidéo dans le dossier " + folder + "\n\nConfirmer ?", "Déplacer la vidéo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    string[] cutted_path = VideoModif.Path.Split('/', '\\');
                    string newPath = folder + "\\" + cutted_path[cutted_path.Length - 1];
                    string oldPath = VideoModif.Path;
                    if (oldPath != newPath)
                    {
                        Video tmpVideo = VideoItem.Create_Copy();
                        tmpVideo.Path = newPath;
                        File.Copy(oldPath, newPath, true);
                        Change_Video(tmpVideo, false);
                        Thread.Sleep(1500);
                        File.Delete(oldPath);
                    }
                }
            }
        }

        /// <summary>
        /// Change the video's folder during the move video action.
        /// </summary>
        /// <returns>The path of the choosen folder.</returns>
        private string Choose_Folder()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog()
            {
                InitialDirectory = new FileInfo(configReader.DestinationDir).FullName,
                IsFolderPicker = true
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            CommonFileDialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == CommonFileDialogResult.Ok)
            {
                return dialog.FileName;
            }

            return "";
        }

        /// <summary>
        /// Change the video's file during the relink video action.
        /// </summary>
        /// <returns>The choosen file name.</returns>
        private string Choose_File()
        {
            String fileExtension = "Video Files|*.avi;*.webm;*.mkv;*.flv;*.vob;*.ogg;*.wmv;*.mp4;*.mpg;*.mpeg;*.mov";
            OpenFileDialog dialog = new OpenFileDialog()
            {
                InitialDirectory = new FileInfo(configReader.DestinationDir).FullName,
                Filter = fileExtension
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            DialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == DialogResult.OK)
            {
                return dialog.FileName;
            }

            return "";
        }

        /// <summary>
        /// The delete video action (button : "Supprimer").
        /// </summary>
        /// <param name="window">The current window.</param>
        public void Delete_Video(Window window)
        {
            if (VideoModif.Readable)
            {
                DialogResult choice = System.Windows.Forms.MessageBox.Show("Vous vous apprêtez à supprimer cette fiche.\nSouhaitez-vous conserver la vidéo ?\nElle sera alors déplacée dans le dossier défini dans les paramètres", "Supprimer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                if (choice == DialogResult.Yes)
                {
                    String[] path = VideoModif.Path.Split('\\');
                    String fileName = path[path.Length - 1];
                    File.Copy(VideoModif.Path, configReader.DeleteDir + "\\" + fileName, true);
                }
                if (choice != DialogResult.Cancel)
                {
                    mdb.Delete_Video(VideoItem);
                    window.Close();
                    File.Delete(VideoModif.Path);
                    lvvm.RaisePropertyChanged("VideoInDatabase");
                }
                if (window != null && choice != DialogResult.Cancel)
                {
                    window.Close();
                }
            }
            else
            {
                DialogResult choice = System.Windows.Forms.MessageBox.Show("Vous vous apprêtez à supprimer cette fiche.\nConfirmer ?", "Supprimer", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (choice == DialogResult.OK)
                {
                    mdb.Delete_Video(VideoItem);
                    if (window != null)
                    {
                        window.Close();
                        lvvm.RaisePropertyChanged("VideoInDatabase");
                    }
                }
            }
        }

        /// <summary>
        /// Default method for the change video action (button : "Valider").
        /// </summary>
        public void Change_Video()
        {
            Change_Video(VideoModif, true);
        }

        /// <summary>
        /// To string method.
        /// </summary>
        /// <returns>The selected video's title.</returns>
        override public string ToString()
        {
            return VideoItem.Title;
        }

        /// <summary>
        /// Second change video method, takes a new video.
        /// </summary>
        /// <param name="videoToChange">The new video to change.</param>
        /// <param name="askForModif">Ask if the modification is wanted.</param>
        public void Change_Video(Video videoToChange, bool askForModif)
        {
            mdb.Modify_Video(VideoItem, videoToChange);
            VideoItem = videoToChange;
            VideoModif = videoToChange.Create_Copy();
            lvvm.RaisePropertyChanged("VideoInDatabase");
        }

        /// <summary>
        /// During the window's shutdown, ask to the user if he would like to valid his changes.
        /// </summary>
        public void On_Quit()
        {
            if (Check_Modifications())
            {
                DialogResult msg = System.Windows.Forms.MessageBox.Show("Des modifications ont été détectées, souhaitez-vous les enregistrer ?", "Modifications détectées", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (msg == DialogResult.Yes)
                {
                    Change_Video(VideoModif, false);
                }
                lvvm.RaisePropertyChanged("VideoInDatabase");
            }
        }

        /// <summary>
        /// Check if the modfication are made for the current video.
        /// </summary>
        /// <returns>True if this is the case, false otherwise.</returns>
        public bool Check_Modifications()
        {
            return !(VideoItem.Description.Equals(VideoModif.Description) && VideoItem.Year.Equals(VideoModif.Year) && VideoItem.Title.Equals(VideoModif.Title));
        }
    }
}