﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;

namespace Video_Browser_Project
{
    /**
     * ListVideoViewModel class
     * View Model for the main windows which contain the video list
     */
    public class ListVideoViewModel : INotifyPropertyChanged
    {
        private static MongoDB_Controller mdb;
        private string _search = "";
        private Video_Modification current_playing_video;

        /// <summary>
        /// Return the list of video which matches with the research
        /// </summary>
        public List<Video> VideoInDatabase
        {
            get
            {
                List<Video> list = mdb.Search_Video(_search);
                list.Sort(Comparer<Video>.Create((a, b) => a.Title.CompareTo(b.Title))); // This line sort the list but doesn't seem to work at first launch of the application
                return list;
            }
        }

        /// <summary>
        /// Get and set the selected video.
        /// </summary>
        public Video Selected_Video { get; set; }

        /// <summary>
        /// Get and set the search value, change the property in consequence.
        /// </summary>
        public string Search
        {
            get => _search;
            set
            {
                _search = value;
                RaisePropertyChanged("VideoInDatabase");
            }
        }

        /// <summary>
        /// Initialize a MongoDB object and a timer for avoid the player to crash when videos are quickly changed.
        /// </summary>
        public ListVideoViewModel()
        {
            mdb = MongoDB_Controller.GetInstance();
            Init_Loading_Timer();
        }

        /// <summary>
        /// The event for alerting the view that a parameter is changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Send a PropertyChanged Event for the attribute named by "PropertyName".
        /// </summary>
        /// <param name="propertyName">the property name attribute</param>
        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // VIEW FUNCTIONS

        /// <summary>
        /// Open a video window with all informations.
        /// </summary>
        public void Open_Video()
        {
            double top = 60;
            double left = 60;
            if (current_playing_video != null)
            {   // If a video window exist
                top = current_playing_video.Top; // Then we save the top and the left position for reopening it at the exact same spot
                left = current_playing_video.Left;
                current_playing_video.Close(); //Then we close the current video window

            }
            current_playing_video = new Video_Modification(Selected_Video, this)
            {
                Top = top, // set the top
                Left = left // set the left
            }; // Reopening the new one
            current_playing_video.Show(); // Then show it to the user
        }

        // Timer for avoiding crashes of the app when there is a fast video switching
        private System.Windows.Forms.Timer loading_timer;
        private int _counter_before_load_video;

        /// <summary>
        /// Init a timer for temporize the launch of the window avoiding crashes.
        /// </summary>
        private void Init_Loading_Timer()
        {
            loading_timer = new System.Windows.Forms.Timer();
            loading_timer.Tick += new EventHandler(Check_Loading);
            loading_timer.Interval = 150; // in miliseconds
        }

        /// <summary>
        /// Reset the counter when this function is launch and start the timer if it is not launched.
        /// </summary>
        public void Start_Loading_Timer()
        {
            Console.WriteLine("Start loading timer");
            _counter_before_load_video = 2;
            Console.WriteLine("Timer enabled : " + loading_timer.Enabled);
            if (!loading_timer.Enabled)
            {
                loading_timer.Start();
            }
        }

        /// <summary>
        /// Each tick of the previous timer, this function is launched. If the counter value is zero, it open the video.
        /// </summary>
        /// <param name="sender">The affected object</param>
        /// <param name="e">The applied event</param>
        private void Check_Loading(object sender, EventArgs e)
        {
            if (_counter_before_load_video <= 0)
            {
                Open_Video();
                loading_timer.Stop();
            }
            else
            {
                _counter_before_load_video--;
            }
        }

        // LAUNCH CONFIG WINDOW

        public ICommand LaunchConfigurationWindow => new RelayCommand(Config_Window);

        /// <summary>
        /// Launch the configuration window, then update the video list.
        /// </summary>
        public void Config_Window()
        {
            Configuration configuration = new Configuration();
            configuration.ShowDialog();
            RaisePropertyChanged("VideoInDatabase");
        }

        // LAUNCH ADD VIDEO WINDOW

        public ICommand AddVideoWindow => new RelayCommand(Add_Video_Window);

        /// <summary>
        /// Launch the add video window, then update the video list.
        /// </summary>
        public void Add_Video_Window()
        {
            AddVideo add_video = new AddVideo();
            try
            {
                add_video.ShowDialog();
            }
            // Gérer mieux l'exception
            catch (System.IO.IOException)
            {
                System.Windows.Forms.MessageBox.Show("Impossible de déplacer la vidéo dans le même dossier", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            RaisePropertyChanged("VideoInDatabase");
        }

        // COPY LINK FUNCTIONS

        /// <summary>
        /// Copy the path of the video in the clipboard.
        /// </summary>
        internal void Copy_Link()
        {
            System.Windows.Forms.Clipboard.SetText(Selected_Video.Path);
            System.Windows.Forms.MessageBox.Show("Chemin de la vidéo copié dans le presse papier", "Chemin copié", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        // PURGE FUNCTION

        /// <summary>
        /// Open a File Dialog for the user, he will give the filename he want to save the XML.
        /// </summary>
        /// <returns>The XML file name.</returns>
        private static string Get_Savefile_XML()
        {
            SaveFileDialog dialog = new SaveFileDialog()
            {
                AddExtension = true,
                Filter = "(*.xml)|*.xml"
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            DialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == DialogResult.OK)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Change the video's folder during the move video action.
        /// </summary>
        /// <returns>The path of the choosen folder.</returns>
        private static string Get_Save_Folder()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog()
            {
                IsFolderPicker = true
            };

            Window window = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

            CommonFileDialogResult dialogResult = dialog.ShowDialog();

            window.Focus();
            if (dialogResult == CommonFileDialogResult.Ok)
            {
                return dialog.FileName;
            }

            return "";
        }

        /// <summary>
        /// Save the selected videos in the choosen folder.
        /// </summary>
        /// <param name="selectedItems">The selected videos</param>
        public void Purge_Selected_Videos_Folder(List<Video> selectedItems)
        {
            foreach (Video item in selectedItems)
            {
                if (!item.Readable)
                {
                    System.Windows.Forms.MessageBox.Show("Le fichier " + item.Path + " n'existe pas", "Erreur d'exportation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
            }
            string folder = Get_Save_Folder();
            if (folder != "")
            {
                DialogResult choice = System.Windows.Forms.MessageBox.Show("Voulez-vous enregistrer les vidéos dans le dossier: " + folder + " ?", "Enregistrer les vidéos", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (choice == DialogResult.Yes)
                {
                    foreach (Video item in selectedItems)
                    {
                        string[] cutted_path = item.Path.Split('/', '\\');
                        string file_foler = cutted_path[cutted_path.Length - 2];
                        string file = cutted_path[cutted_path.Length - 1];
                        if (!Directory.Exists(folder + "\\" + file_foler))
                        {
                            Directory.CreateDirectory(folder + "\\" + file_foler);
                        }
                        string newPath = folder + "\\" + file_foler + "\\" + file;
                        string oldPath = item.Path;
                        Video tmpVideo = item.Create_Copy();
                        tmpVideo.Path = newPath;
                        try
                        {
                            File.Copy(oldPath, newPath, true);
                        }
                        catch (System.IO.IOException)
                        {
                            System.Windows.Forms.MessageBox.Show("Veuillez sélectionner un dossier différérent de celui d'origine", "Erreur d'exportation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            return;
                        }
                        Thread.Sleep(1500);
                        mdb.Delete_Video(item);
                        File.Delete(oldPath);
                    }
                    System.Windows.Forms.MessageBox.Show("Les vidéos ont été exportées", "Exportation réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Purge_Selected_Videos_XML(selectedItems);
                RaisePropertyChanged("VideoInDatabase");
            }
        }

        /// <summary>
        /// Export the selected videos in an XML file.
        /// </summary>
        /// <param name="selectedItems">The selected videos</param>
        private void Purge_Selected_Videos_XML(List<Video> selectedItems)
        {
            string xmlFile = Get_Savefile_XML();
            if (xmlFile != null)
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlElement root = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, root);
                XmlElement videos = xmlDoc.CreateElement("video_list");
                foreach (Video video in selectedItems)
                {
                    XmlElement video_node = xmlDoc.CreateElement("video");
                    XmlElement title = xmlDoc.CreateElement("title");
                    XmlText titleText = xmlDoc.CreateTextNode(video.Title);
                    XmlElement year = xmlDoc.CreateElement("year");
                    XmlText yearText = xmlDoc.CreateTextNode(video.Year);
                    XmlElement path = xmlDoc.CreateElement("path");
                    XmlText pathText = xmlDoc.CreateTextNode(video.Path);
                    XmlElement desc = xmlDoc.CreateElement("desc");
                    XmlText descText = xmlDoc.CreateTextNode(video.Description);
                    title.AppendChild(titleText);
                    year.AppendChild(yearText);
                    path.AppendChild(pathText);
                    desc.AppendChild(descText);
                    video_node.AppendChild(title);
                    video_node.AppendChild(path);
                    video_node.AppendChild(year);
                    video_node.AppendChild(desc);
                    videos.AppendChild(video_node);
                }
                xmlDoc.AppendChild(videos);
                xmlDoc.Save(xmlFile);
            }
        }
    }
}
