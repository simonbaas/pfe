﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.IO;

namespace Video_Browser_Project
{
    /**
     * The main Video Class
     * Contains all video's attribute and methods
     */
    [BsonIgnoreExtraElements]
    public class Video
    {
        /// <summary>
        /// Video's title
        /// </summary>
        [BsonElement("title")]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Video's year
        /// </summary>
        [BsonElement("year")]
        public string Year
        {
            get;
            set;
        }

        /// <summary>
        /// Video's description
        /// </summary>
        [BsonElement("description")]
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Video's path
        /// </summary>
        [BsonElement("path")]
        public string Path
        {
            get;
            set;
        }

        /// <summary>
        /// If the video is readable (not readable if the file isn't exist for exemple).
        /// </summary>
        public bool Readable => File.Exists(Path);

        /// <summary>
        /// The main constructor of the Video's class.
        /// </summary>
        /// <param name="title">Video's name./title</param>
        /// <param name="year">Video's year.</param>
        /// <param name="description">Video's description.</param>
        /// <param name="path">Video's path.</param>
        public Video(String title, String year, String description, String path)
        {
            Title = title;
            Year = year;
            Description = description;
            Path = path;
        }

        /// <summary>
        /// The second Video's constructor.
        /// </summary>
        public Video()
        {
            Title = "";
            Year = "";
            Description = "";
            Path = "";
        }

        /// <summary>
        /// Create a copy of the video.
        /// </summary>
        /// <returns>
        /// A new Video object.
        /// </returns>
        public Video Create_Copy()
        {
            return new Video(Title, Year, Description, Path);
        }

        /// <summary>
        /// Video's toString methods
        /// </summary>
        /// <returns> A string with all of the video's information.</returns>
        public override string ToString()
        {
            String res;
            res = "Title : " + Title + "\n";
            res += "Year : " + Year + "\n";
            res += "Description :\n" + Description + "\n";
            res += "Description :\n" + Description + "\n";
            return res;
        }
    }
}
