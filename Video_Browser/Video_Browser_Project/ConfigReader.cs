﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Video_Browser_Project
{
    /**
     * ConfigReader class
     * Contains all informations about the configuration, import/export methods and the different directories
     */
    internal class ConfigReader
    {
        private static readonly string configFile = "configuration.xml";
        private static ConfigReader instance = null;
        private static XmlDocument xmlConfig;
        private static XmlNode _sourceDir;
        private static XmlNode _destDir;
        private static XmlNode _deleteDir;

        // The path of the directory where the location of the videos the user want to add. 
        // Default will be the video folder of the application.
        private readonly string DEFAULT_SOURCEDIR = Path.GetDirectoryName(Application.ExecutablePath) + "\\videos";
        private readonly string DEFAULT_DESTDIR = Path.GetDirectoryName(Application.ExecutablePath) + "\\videos";
        private readonly string DEFAULT_DELETEDIR = Path.GetDirectoryName(Application.ExecutablePath) + "\\videos\\delete";

        /// <summary>
        /// The path of the source directory.
        /// </summary>
        public string SourceDir
        {
            set
            {
                XmlWriter xmlw = XmlWriter.Create(configFile);
                _sourceDir.InnerText = value;
                xmlConfig.WriteTo(xmlw);
                xmlw.Close();
            }
            get
            {
                string path = _sourceDir.InnerText;
                if (!Directory.Exists(path)) // Check if the directory exists, otherwise, it will return to default dir
                {
                    if (!Directory.Exists(DEFAULT_SOURCEDIR))
                    {
                        Directory.CreateDirectory(DEFAULT_SOURCEDIR);
                    }
                    SourceDir = DEFAULT_SOURCEDIR;
                }
                return _sourceDir.InnerText;
            }
        }

        /// <summary>
        /// The path of the destination for the videos.
        /// </summary>
        /// <remarks>
        /// <para>That is where the window will open where we have to choose a destination folder.</para>
        /// <para>But the user will be allow to choose and other folder from there.</para>
        /// <para>Default will be the video folder of the application.</para>
        /// </remarks>
        public string DestinationDir
        {
            set
            {
                XmlWriter xmlw = XmlWriter.Create(configFile);
                _destDir.InnerText = value;
                xmlConfig.WriteTo(xmlw);
                xmlw.Close();
            }
            get
            {
                string path = _destDir.InnerText;
                if (!Directory.Exists(path)) // Check if the directory exists, otherwise, it will return to default dir
                {
                    if (!Directory.Exists(DEFAULT_DESTDIR))
                    {
                        Directory.CreateDirectory(DEFAULT_DESTDIR);
                    }
                    DestinationDir = DEFAULT_DESTDIR;
                }
                return _destDir.InnerText;
            }
        }

        /// <summary>
        /// The folder where deleted video will be send.
        /// The default folder will be the video/deleted folder from the application.
        /// </summary>
        public string DeleteDir
        {
            set
            {
                XmlWriter xmlw = XmlWriter.Create(configFile);
                _deleteDir.InnerText = value;
                xmlConfig.WriteTo(xmlw);
                xmlw.Close();
            }
            get
            {
                string path = _deleteDir.InnerText;
                if (!Directory.Exists(path)) // Check if the directory exists, otherwise, it will return to default dir
                {
                    if (!Directory.Exists(DEFAULT_DELETEDIR))
                    {
                        Directory.CreateDirectory(DEFAULT_DELETEDIR);
                    }
                    DeleteDir = DEFAULT_DELETEDIR;
                }
                return _deleteDir.InnerText;
            }
        }

        private ConfigReader()
        {
            if (!File.Exists(configFile))
            {
                GenerateConfig();
            }
            LoadConfig();
        }

        /// <summary>
        /// Load the configuration from the XML file.
        /// </summary>
        private void LoadConfig()
        {
            xmlConfig = new XmlDocument();
            xmlConfig.Load(configFile);
            _sourceDir = xmlConfig.DocumentElement.SelectSingleNode("//config/videoSourceDir/node()");
            _destDir = xmlConfig.DocumentElement.SelectSingleNode("//config/defaultDestinationDirectory/node()");
            _deleteDir = xmlConfig.DocumentElement.SelectSingleNode("//config/deleteDir/node()");
        }

        /// <summary>
        /// Generate a config file if the file doesn't exist.
        /// </summary>
        private void GenerateConfig()
        {
            String content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                          "<config>\n" +
                          "    <videoSourceDir>" + DEFAULT_SOURCEDIR + "</videoSourceDir>\n" +
                          "    <defaultDestinationDirectory>" + DEFAULT_DESTDIR + "</defaultDestinationDirectory>\n" +
                          "    <deleteDir>" + DEFAULT_DELETEDIR + "</deleteDir>\n" +
                          "</config>\n";
            File.WriteAllText(configFile, content);
        }

        /// <summary>
        /// Implement the singleton pattern.
        /// </summary>
        /// <returns>A single instance of the class.</returns>
        public static ConfigReader GetInstance()
        {
            if (instance == null)
            {
                instance = new ConfigReader();
            }
            return instance;
        }
    }
}
