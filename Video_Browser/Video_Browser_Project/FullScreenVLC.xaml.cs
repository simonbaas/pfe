﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Video_Browser_Project
{
    /// <summary>
    /// Logique d'interaction pour FullScreenVLC.xaml
    /// </summary>
    public partial class FullScreenVLC : Window
    {
        public bool Playing { get; private set; }
        public long Timer { get; private set; }
        public long TimeOnStop { get; private set; }
        public FullScreenVLC(String videoPath, long time)
        {
            InitializeComponent();
            VLC_Control.VideoPath = videoPath;
            VLC_Control.Change_Video_Time(time);
            Timer = time;
            Playing = VLC_Control.VideoControl.SourceProvider.MediaPlayer.IsPlaying();
        }

        public void PlayFullScreen()
        {
            if (!Playing) // Keep the same state before when enter in the fullscreen
            {
                VLC_Control.VideoControl.SourceProvider.MediaPlayer.Pause();
                /*VLC_Control.Change_Video_Time(timer);*/
                /*VLC_Control.Play.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/Images/playVLC.PNG"));*/
            }
        }

        public void Change_Video_Time_FullScreen(long time)
        {
            System.Threading.Thread.Sleep(10);
            VLC_Control.VideoControl.SourceProvider.MediaPlayer.Time = time;
        }

        public void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TimeOnStop = VLC_Control.VideoControl.SourceProvider.MediaPlayer.Time;
            VLC_Control.StopPlayer();
        }
    }
}
