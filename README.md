# PFE - "Trouve-moi vite la vidéo stp !"

## Rapport Technique

Simon Baas - M2 IAGL  
Enseignant: Jean-Claude Tarby  
Année: 2020 - 2021

## Lancement du projet

Le projet est développé autour des technologies .NET à l'aide de Visual Studio.

### Lancer le programme dans VS Studio

La solution Video_Browser.sln permet de lancer le projet dans Visual Studio, celle-ci se trouve dans Video_Browser.
Une fois Visual Studio lancé, il suffit de démarrer l'application grâce à la flèche verte située en haut. L'application nécessite une archive mongodb. Si un message apparaît indiquant "mongodb n'a pas été trouvé", alors il vous suffit de récupérer l'archive zip "mongodb.zip" dans Video_Browser/Video_Browser_Project et de la placer dans le dossier Video_Browser/Video_Browser_Project/bin/Debug. Vous pouvez ensuite relancer l'application, l'archive sera dezipée. Cependant dû à un problème, MongoDB doit être lancé sur la machine avant le lancement de l'application.

### Installer l'application

Visual Studio fournit grâce aux packages NuGet, un utilitaire d'installation. Celui-ci prend en charge tous les éléments permettant le lancement de l'application, mais aussi les éléments comme la librairie VLC, MongoDB ou bien encore les images utilisées.

Afin d'installer l'application, il faut se rendre dans Video_Browser/Video Browser Setup/Debug et lancer setup.exe
L'installateur vous demandera où vous voulez installer le programme. Il est préférable d'installer l'application à un endroit où vous avez le droit de créer un dossier. Une fois l'installation fini, un raccourci "Video Browser" sera créé sur le bureau. L'application nécessite que MongoDB soit installé et configuré sur la machine (dossier C:/data/db). Avant lancement de l'application, MongoDB doit être lancé.

## Conception C#

Le principal outil de conception utilisé est le modèle-vue-vue modèle (MVVM). L'application se décompose en trois parties. Les modèles, les vues et les vues-modèles.

Les modèles sont les éléments contenant toutes les informations de nos objets. Dans l'application, nous avons deux modèles, Video et ConfigReader. Video permet de représenter une vidéo à l'aide de tous ses attributs. ConfigReader permet de stocker tous les attributs relatifs à la configuration, dossier de destination, exportation, importation, ect.

Les vues sont nos interfaces. Nous avons donc la vue principale, la vue de configuration, la vue d'ajout de vidéo, la vue de modification et le lecteur VLC. Afin de relier ces vues aux modèles, nous avons besoin de vue-modèle. Ainsi chaque vue aura une vue-modèle bindée et dans celle-ci, nous retrouverons l'utilisation du modèle.

### Exemple de binding

Par exemple dans la vue AddVideo.xaml, on bind la vue-modèle à l'aide de:

```
<Window.DataContext>
    <local:AddVideoViewModel/>

</Window.DataContext>
```

Ainsi par exemple pour la listBox, chaque élément possède un binding afin qu'il puisse être récupéré par la vue-modèle pour ensuite être traité.

```
<ListBox Name="listBox1" SelectedItem="{Binding Selected_Video}" ...>
    ...
</ListBox>
```

Le binding intervient directement sur notre function getter/setter dans AddVideoViewModel.cs

```
public String Selected_Video
{
    get => _selectedVideo;
    set
    {
        _selectedVideo = value;
        if (_selectedVideo != null)
        {
            _selectedVideo_copy = String.Copy(_selectedVideo);
        }
        Start_Loading_Timer();
        RaisePropertyChanged("Selected_Video");
    }
}
```

## Packages NuGet

L'utilisation de Visual Studio permet d'intégrer facilement des plug-in, ici appelé package NuGet. Dans l'application, deux packages sont utilisés, le package VLC.NET et le package MongoDB. Ces packages sont directement intégrés dans la solution de l'application. VLC.NET permet d'intégrer le lecteur vidéo VLC dans la fenêtre d'ajout de vidéo et de modification. Le package MongoDB permet d'utiliser facilement la base de données MongoDB à l'aide d'un controleur lors de l'ajout, modification et suppression de vidéos dans la base.

## Les classes de l'application

### Video.cs

Video.cs est le modèle permettant de représenter les vidéos. Il possède différents attributs, title, year, description et path. Tous ces attributs sont définis à l'aide de BsonElement propre au package MongoDB, et qui permet l'entrée en base de chaque vidéo.
On vérifie qu'une vidéo est lisible, si le fichier existe par rapport au chemin (path donné).
Il est possible de créer une copie de la vidéo, notamment lorsque l'on ajoute les vidéos, avant de les supprimer dans de répertoire d'origine.

### ConfigReader.cs

Tout comme Video.cs, ConfigReader.cs est un modèle. Il permet de contenir les informations de la configuration. On retrouve des attributs définissants le répertoire source des vidéos, le répertoire de destination et le répertoire par défaut des vidéos supprimées. Cette classe a besoin d'être instanciée une seule fois, c'est donc un singleton.

ConfigReader.cs permet à l'aide de ces informations, de construire les fichiers xml nécessaire à l'import et à l'export.
En utilisant System.Xml et les classes XmlDocument, XmlNode et XmlWriter on peut facilement construire un fichier XML.

### PlayerVLC.xaml

PlayerVLC.xaml est la vue permettant de visualiser le lecteur vidéo VLC. Grâce au package NuGet VLC.NET, la classe PLayerVLC permet d'utiliser un UserControler. Le UserControler est un module pouvant être intégré dans une autre vue. Ainsi, c'est grâce à cela qu'on peut intégrer facilement le lecteur VLC dans PlayerVLC.xaml.

Dans le lecteur VLC, une barre de temps et des boutons de lecture/pause ont été rajoutés, ainsi qu'un bouton fullscreen. Lorsque l'on clique sur le bouton fullscreen, ça ouvre le vue FullScreen.xaml.

La vue FullScreen.xaml est juste une fenêtre agrandie et intègre directement PlayerVLC.xaml.

### MongoDB_Controler.cs

MongoDB_Controler.cs est un contrôleur implémentant les fonctions nécessaires à l'ajout, modification et suppression des vidéos dans la base. Comme ConfigReader.cs, MongoDB_Controller.cs est un singleton car il a besoin d'être instancié une seule fois.

Dans ce contrôleur, on définit la base de données "video_broser", ainsi que la collection "video". Si celles-ci n'existent pas, elles sont donc crées. À l'aide des éléments BsonElement définient dans le modèle Video.cs, le contrôleur peut ajouter facilement les vidéos à la base.

Par exemple, au-dessus de l'attribut title d'une vidéo nous avons:

```
[BsonElement("title")]
```

Le contrôleur permet aussi de nettoyer la base de données, et de la shutdown. La recherche de vidéo de la page d'accueil se fait aussi à partir du contrôleur.

### RelayCommand.cs

RelayCommand.cs permet des binder les commandes aux boutons. Par exemple le bouton d'ajout de vidéo dans la fenêtre principale:

```
<MenuItem Name="AddVideoButton" Header="Ajouter des vidéos" Command="{Binding AddVideoWindow}"/>
```

Dans la vue-modèle on retrouve le RelayCommand

```
public ICommand AddVideoWindow => new RelayCommand(Add_Video_Window);
```

Avec bien sur la fonction Add_Video_Window

```
public void Add_Video_Window()
{
    AddVideo add_video = new AddVideo();
    try
    {
        add_video.ShowDialog();
    }
    // Gérer mieux l'exception
    catch (System.IO.IOException)
    {
        MessageBox.Show("Impossible de déplacer la vidéo dans le même dossier");
    }
    RaisePropertyChanged("VideoInDatabase");
}
```

Néanmoins, certaines actions de la vue ne peuvent pas être bindée comme une ICommand, généralement cela concerne la gestion d'événements. Lorsque l'on effectue un clic gauche sur un bouton, derrière, du code xaml.cs peut être exécuté.
Par exemple pour la gestion du Drag & Drop, les éléments insérés n'étaient pas supprimés à l'ajout. Il n'était pas possible de le faire côté vue-modèle, il fallait donc les supprimer une fois le bouton d'ajout cliqué. Cela puvait causer des soucis car l'élément est supprimé avant que les informations nécessaires à son ajout en base soient récupérées grâce aux fonctions du vue-modèle. Cependant, toutes les exceptions ont été gérées.

### Video_Browser.xaml

Video_Browser.xaml représente la vue principale de l'application. On y retrouve un bouton permettant d'accéder à la vue d'ajout de vidéo et à la vue de configuration. Le tableau contenant l'ensemble des vidéos est aussi présent, et on peut lancer une recherche dessus comme filtrer par colonne.

### ListVideoViewModel.cs

ListVideoViewModel.cs est la vue-modèle bindé à la vue Video_Browser.xaml. Cette vue-modèle permet de récupérer la liste de vidéo. Elle récupère aussi le MongoDB_Controller et utilise sa fonction de recherche. Une fonction permettant d'ouvrir les vidéos est aussi définie.
L'ouverture des fenêtres d'ajout de vidéo et d'ouverture de la fenêtre de configuration se fait via des boutons. Afin d'associer une fonction aux boutons, on utilise la classe RelayCommand.cs.

Par exemple la fonction d'ouverture de la fenêtre de configuration

```
public void Config_Window()
{
    Configuration configuration = new Configuration();
    configuration.ShowDialog();
    RaisePropertyChanged("VideoInDatabase");
}
```

Cette fonction est bindée au bouton grâce à :

```
public ICommand LaunchConfigurationWindow => new RelayCommand(Config_Window);
```

Un timer est utilisé pour éviter au lecteur VLC de charger trop vite les vidéos. Lorsque l'on clique sur une vidéo dans la liste, la vidéo va être lue par le lecteur VLC grâce à la vue PlayerVLC.cs. Cependant, lorsque l'on clique très rapidement sur les vidéos, cela entraîne un rechargement du player à de nombreuses reprises et il est possible qu'il n'ait pas le temps de se réinitialiser.
Le timer permet donc lorsque l'on clique sur une vidéo, d'attendre un peu moins d'une seconde avant de charger la vidéo.
Ce timer est présent dans les vues utilisant le lecteur VLC.

## Problèmes rencontrés

L'utilisation de MongoDB au sein de l'application a posé pas mal de soucis et possède encore un problème. En effet, le lancement se fait à l'aide d'un processus lancer directement dans le code Video_Browser.xaml.cs au lancement de l'application. On vérifie si l'exécutable mongod.exe est bien présent dans le projet. Sinon, si on trouve mongo.zip alors on l'extrait. Cependant le lancement de mongod.exe lance mongo mais la fenêtre se ferme tout de suite. Cela pourrait être dû à l'ajout d'argument spécifiant le répertoire db afin de stocker les collections. Cependant même en rajoutant cela, mongo ne se lance pas. L'application attend donc une connexion mongo qu'il faudra lancer au préalable.

Comme évoqué précédemment, la suppression des éléments du drag and drop, ne peut pas se faire du côté vue-modèle. Ainsi elle est gérée directement lors du clic sur le bouton d'ajout. Le fait de ne pas utiliser le vue-modèle n'est pas optimal, cependant cela ne cause aucun bug et les différentes exceptions ont été gérées.

## Les points d'améliorations

Le projet est complètement opérationnel, cependant quelques points d'améliorations peuvent être envisagés.

Il peut être intéressant de changer peut-être le fullscreen. En effet le problème de lecture/pause vient du fait que nous avons deux vues différentes PlayerVL.xaml et FullScreenVLC.xaml. Ainsi nous avons donc deux états différents et il est difficile de garder le même état lors du changement de vue (clique sur le bouton fullscreen). On pourrait peut-être avoir une uniquement vue contenant le lecteur.

Le lancement de MongoDB est aussi un point à revoir. Comme énoncé précédemment, le lancement se fait bien, cependant, le processus doit manquer d'un paramètre car la fenêtre se ferme directement après.

Ensuite, l'intérêt est de reprendre cette application afin de développer une application de lecture de photo. Il faudrait donc changer le lecteur VLC et mettre en place un système de diaporama.
