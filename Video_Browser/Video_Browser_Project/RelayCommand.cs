﻿using System;
using System.Windows.Input;

namespace Video_Browser_Project
{
    internal class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action _method;

        public RelayCommand(Action method)
        {
            _method = method;
        }

        public bool CanExecute(object param)
        {
            return true;
        }

        public void Execute(object param)
        {
            _method.Invoke();
        }


    }
}
